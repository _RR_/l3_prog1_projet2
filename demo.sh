#!/bin/sh

# variable to use in read because we use a way to old sh (and not bash or zsh)
UNUSED=""

alias wait_for_user='echo "Press enter..." && read UNUSED && sleep 0.7'
# Basic demo
#Clear the command console.

echo "************************************************************************"
echo "\tDemonstration of Project-2"
echo "by Bastide/Coudray/Duron/Piau"

echo "During the demo please press enter each time you want to make a step"

echo "\n\n"

# entering source dir
echo "Entering source directory"
cd raytracing

#Clear compilation files
echo "Clean the compilation files."
(set -x; make -s clean >/dev/null 2>&1)

#Compile files
echo "Compile needed files. Please wait."
(set -x; make -s >/dev/null 2>&1)

echo "\n\n\n"

echo "Start main demo:"

wait_for_user

#Start the demonstration script
echo "1. Basic black and white render"
(set -x; ./raytracing -r 0)

echo "\n\n\n"
echo "2. Color aware render"
wait_for_user
(set -x; ./raytracing -r 1)

echo "\n\n\n"
echo "3. Diffusion only render"
wait_for_user
(set -x; ./raytracing -r 2)

echo "\n\n\n"
echo "4. Diffusion and reflexion and transparency aware render"
wait_for_user
(set -x; ./raytracing -i models/balls_with_transparency.csv -r 3)
wait_for_user
(set -x; ./raytracing -i models/mirror_balls.csv -r 3)

echo "\n\n\n"
echo "5. Polyhedra support"
wait_for_user
(set -x; ./raytracing -i models/three_cubes.csv -r 3)

echo "\n\n\n"
echo "6. Polynomial equation support (up to 2nd degree)"
wait_for_user
(set -x; ./raytracing -i models/parabolic_droplet.csv -r 3)
wait_for_user
(set -x; ./raytracing -i models/saddle_and_mirror.csv -r 3)

echo "We have a cli-args parsing and csv world loader for fast world change !"
wait_for_user
(set -x; ./raytracing -h)

echo "Thanks for your attention. We will now load a heavy polyhedra if you have time."
wait_for_user
(set -x; ./raytracing -i models/dinosaur.csv -r 3)
