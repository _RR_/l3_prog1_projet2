
PLATFORM := Unknown

SYSTEM := $(shell uname -s)
ifeq ($(SYSTEM), Darwin)
	PLATFORM := Darwin
else
	ifeq ($(SYSTEM), Linux)
		PLATFORM := $(shell lsb_release -si)
	endif
endif

ifeq ($(PLATFORM), Unknown)
$(error Unknown platform: Please check Makefile configuration)
endif

$(info Platform: $(PLATFORM))

###########################################################

ifeq ($(PLATFORM), Ubuntu)
# Ubuntu
# sudo apt install libsfml-dev
SFML_INSTALL_DIR := /usr
SFML_INCLUDE_DIR := $(SFML_INSTALL_DIR)/include
SFML_LIB_DIR := $(SFML_INSTALL_DIR)/lib/x86_64-linux-gnu
CC := g++
endif

###########################################################

ifeq ($(PLATFORM), Arch)
# Ubuntu
# pacman -S lsb-release sfml
SFML_INSTALL_DIR := /usr
SFML_INCLUDE_DIR := $(SFML_INSTALL_DIR)/include
SFML_LIB_DIR := $(SFML_INSTALL_DIR)/lib/x86_64-linux-gnu
CC := g++
endif

###########################################################

ifeq ($(PLATFORM), Darwin)
# MacOS 10.13
# brew install sfml
SFML_INSTALL_DIR := /usr/local
SFML_INCLUDE_DIR := $(SFML_INSTALL_DIR)/include
SFML_LIB_DIR := $(SFML_INSTALL_DIR)/lib
# Warning: SFML is compiled with clang on Mac, not gcc
CC := clang++
endif

###########################################################

ifeq ($(PLATFORM), CentOS)
# CentOS
SFML_INSTALL_DIR := /usr
SFML_INCLUDE_DIR := $(SFML_INSTALL_DIR)/include
SFML_LIB_DIR := $(SFML_INSTALL_DIR)/lib/
CC := g++
endif

###########################################################
