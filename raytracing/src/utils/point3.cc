#include "utils/point3.hh"

using namespace std;

Point3::Point3(double x, double y, double z)
    : x(x), y(y), z(z) {
}

double Point3::get_x() const {
    return x;
}
double Point3::get_y() const {
    return y;
}
double Point3::get_z() const {
    return z;
}

Point3 operator +(const Point3& p, const Vector3& v) {
    return Point3(
               p.get_x() + v.get_x(),
               p.get_y() + v.get_y(),
               p.get_z() + v.get_z()
           );
}

Point3 operator -(const Point3& p, const Vector3& v) {
    return Point3(
               p.get_x() - v.get_x(),
               p.get_y() - v.get_y(),
               p.get_z() - v.get_z()
           );
}

Vector3 operator -(const Point3& p1, const Point3& p2) {
    return Vector3(
               p1.get_x() - p2.get_x(),
               p1.get_y() - p2.get_y(),
               p1.get_z() - p2.get_z()
           );
}

bool operator ==(const Point3& p1, const Point3& p2) {
    return (p1.get_x() == p2.get_x() &&
            p1.get_y() == p2.get_y() &&
            p1.get_z() == p2.get_z());
}

bool is(const Point3& p1, const Point3& p2, double precision) {
    Vector3 diff = p1 - p2;
    return (dot(diff,diff) < precision);
}

Point3 min_point(Point3 reference, const vector<Point3>& vPoint) {
    Point3 minpoint = vPoint.at(0);
    for(Point3 point: vPoint) {

        if(compare_vect( point - reference, minpoint - reference))
        {
            minpoint = point;
        }
    }
    return minpoint;
}


std::ostream& operator <<(std::ostream& s, const Point3& p) {
    double x = p.get_x();
    double y = p.get_y();
    double z = p.get_z();
    return s << "[" << x << ", " << y << ", " << z << "]";
}
