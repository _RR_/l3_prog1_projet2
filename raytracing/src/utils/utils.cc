#include "utils/utils.hh"

double fast_expo ( double x, int n) {
    if (n == 0) {
        return 1.;
    } else {
        if (n % 2 == 0) {
            double y = fast_expo ( x, n/2);
            return y*y;
        } else {
            double y = fast_expo (x, (n-1)/2);
            return y*y*x;
        }
    }
}

double get_cos_angle ( const Vector3& v1, const Vector3& v2) {
    double scal = dot (v1, v2);
    if (scal==0.) {
        return 0.;
    }
    double scal_square = scal*scal;
    double norm_v1_square = dot (v1, v1);
    double norm_v2_square = dot (v2, v2);
    double cos_square = scal_square / (norm_v1_square * norm_v2_square);
    return sqrt (cos_square);
}
