#include "utils/vector3.hh"

Vector3::Vector3(double x, double y, double z)
    : x(x), y(y), z(z) {
}

double Vector3::get_x() const {
    return x;
}
double Vector3::get_y() const {
    return y;
}
double Vector3::get_z() const {
    return z;
}

double Vector3::length() const {
    return sqrt(x * x + y * y + z * z);
}

Vector3 operator *(double t, const Vector3& v) {
    double x = t * v.get_x();
    double y = t * v.get_y();
    double z = t * v.get_z();
    return Vector3(x, y, z);
}

Vector3 operator /(const Vector3& v, double t) {
    return (1/t) * v;
}

Vector3 operator +(const Vector3& u, const Vector3& v) {
    double x = u.get_x() + v.get_x();
    double y = u.get_y() + v.get_y();
    double z = u.get_z() + v.get_z();
    return Vector3(x, y, z);
}

Vector3 operator -(const Vector3& u, const Vector3& v) {
    return u + (-v);
}

Vector3 operator -(const Vector3& v) {
    double a = -v.get_x();
    double b = -v.get_y();
    double c = -v.get_z();
    return Vector3(a, b, c);
}


bool operator ==(const Vector3& v1, const Vector3& v2) {
    return (v1.get_x() == v2.get_x() &&
            v1.get_y() == v2.get_y() &&
            v1.get_z() == v2.get_z());
}


bool is(const Vector3& v1, const Vector3& v2, double precision) {
    Vector3 diff = v1 - v2;
    return (dot(diff,diff) < precision);
}

bool compare_vect(const Vector3 v1, const Vector3 v2) {
    return (dot(v1,v1) < dot(v2, v2));
}


double dot(const Vector3& v1, const Vector3& v2) {
    double a1 = v1.get_x();
    double a2 = v2.get_x();
    double b1 = v1.get_y();
    double b2 = v2.get_y();
    double c1 = v1.get_z();
    double c2 = v2.get_z();
    return (a1 * a2) + (b1 * b2) + (c1 * c2);
}

Vector3 cross(const Vector3& v1, const Vector3& v2) {
    double a1 = v1.get_x();
    double a2 = v2.get_x();
    double b1 = v1.get_y();
    double b2 = v2.get_y();
    double c1 = v1.get_z();
    double c2 = v2.get_z();

    double aa = (b1 * c2) - (c1 * b2);
    double bb = (c1 * a2) - (a1 * c2);
    double cc = (a1 * b2) - (b1 * a2);

    return Vector3(aa, bb, cc);
}

Vector3 normalize(Vector3 v) {
    double vl = v.length();
    assert(vl > 0);
    return v / vl;
}

std::ostream& operator <<(std::ostream& s, const Vector3& v) {
    double x = v.get_x();
    double y = v.get_y();
    double z = v.get_z();
    return s << "(" << x << ", " << y << ", " << z << ")";
}
