#include "utils/csvloader.hh"

using namespace std;

CsvLoader::CsvLoader(const string data, const string separator, const string ignored_char):
    file_path(data), separator(separator), ignored_char(ignored_char) {
    entry.resize(255, '*');

}

bool CsvLoader::open() {
    in = ifstream(file_path.c_str());
    if(not(in.is_open())) {
        return false;
    }
    return true;
}

bool CsvLoader::end_stream() {
    return not(in.good());

}

string CsvLoader::get_entry() {
    do {
        getline(in, entry);
    }
    while(entry[0] == '#');
    // ignore commentar
    // ignore space
    entry = CsvLoader::delete_ignored(entry);
    return entry;
}

vector<string> CsvLoader::parse_entry() {
    clog << "Parsing entry:" << entry << endl;
    size_t initial_position = 0;
    size_t old_position = -1;
    vector<string> values;
    // while we are not at the end of file
    while(initial_position != string::npos && initial_position < entry.length()) {
        // seek the next separator
        initial_position = entry.find_first_of(separator, initial_position+1);
        // if we are not at the end of file
        if(initial_position != string::npos) {
            // add entry
            values.push_back(entry.substr(old_position+1, initial_position-old_position-1));
        }
        else {
            values.push_back(entry.substr(old_position+1, entry.length()-1-old_position));
        }
        clog << "\t\tParsed entry:" << values.at(values.size()-1) << endl;
        // save position of the last separator
        old_position = initial_position;
    }
    return values;

}

string CsvLoader::delete_ignored(string line) {
    // Thanks
    // https://stackoverflow.com/questions/83439/remove-spaces-from-stdstring-in-c
    for(char c: ignored_char) {
        std::string::iterator end_pos = std::remove(line.begin(), line.end(), c);
        line.erase(end_pos, line.end());
    }
    return line;
}
