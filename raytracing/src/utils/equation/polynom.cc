#include "utils/equation/polynom.hh"

using namespace std;

Polynom::Polynom (const int deg, const vector<double>& vCoeff) :
    degree(deg), vCoeff(vCoeff) {}

Polynom::Polynom (const int deg) : degree(deg), vCoeff(vector<double> (deg+1) ) {}

Polynom::Polynom () : degree(0), vCoeff(vector<double> (1,1.)) {}

double Polynom::get_coef (int i) const {
    if (i > degree) {
        return 0.;
    }
    return vCoeff.at(i);
}

Polynom Polynom::operator * (const Polynom& p) const {
    // initialize the result

    int degree_poly = degree + p.degree;
    vector<double> new_vCoeff(degree_poly + 1);

    // set the coefficients of the result

    for (int i = 0; i < degree +1; i++) {
        for (int j = 0; j < p.degree +1; j++) {
            new_vCoeff.at(i+j) += get_coef(i)*p.get_coef(j);
        }
    }

    return Polynom ( degree_poly, new_vCoeff);
}

Polynom Polynom::operator + (const Polynom& p) const {
    // initialize the result

    int degree_pol;
    if (degree < p.degree) {
        degree_pol = p.degree;
    }
    else {
        degree_pol = degree;
    }
    vector<double> new_vCoeff(degree_pol + 1,0.);

    // set the coefficients of the result

    for (int i = 0; i < degree_pol+ 1; i++) {
        if (i <= degree) {
            new_vCoeff.at(i) = get_coef(i);
        }
        if (i <= p.degree) {
            new_vCoeff.at(i) += p.get_coef(i);
        }
    }

    return Polynom( degree_pol, new_vCoeff);
}

Polynom operator * (double lambda, const Polynom& p) {
    // initialize the result

    int degree_poly = p.get_degree();
    vector<double> new_vCoeff(degree_poly + 1);

    // set the coefficients of the result

    for (int i = 0; i < degree_poly +1; i++) {
        new_vCoeff.at(i) = lambda * p.get_coef(i);
    }
    return Polynom(degree_poly, new_vCoeff);
}

Polynom operator ^ (const Polynom& p, int n) {
    if (n == 0) {
        return Polynom();
    }
    else {
        if (n%2 == 0) {
            Polynom pol = operator ^ (p, n/2);
            return pol * pol;
        }
        else {
            Polynom pol = operator ^ (p, (n-1)/2);
            return pol * pol * p ;
        }
    }
}

int Polynom::get_degree() const {
    return degree;
}

vector<double> Polynom::get_vCoeff() const {
    return vCoeff;
}

double Polynom::get_first_root () const {
    // the function return, when it exist, the first positive root of the polynom
    // otherwise, it returns -1

    if (degree == 0) {
        return -1;
    }

    if (degree == 1) {
        double a = get_coef(1);
        // verifie if the polynom is of degree 1, if it is of degree 0, there is no solution
        if (a == 0) {
            return -1;
        }
        double b = get_coef(0);
        double res = -b / a;
        if (res > PRECISION) {
            return res;
        }
        else {
            return -1;
        }
    }

    if (degree == 2) {
        double a = get_coef ( 2);
        // verifie if the polynom is of degree 2, if not, it reexecute
        // itself on the equivalent polynom of degree 1
        if (a==0) {
            vector<double> new_vCoeff = {get_coef(0), get_coef(1)};
            Polynom pol (1, new_vCoeff);
            return pol.get_first_root();
        }
        double b = get_coef(1);
        double c = get_coef(0);
        double discriminant = b*b - 4. * a * c;
        if (discriminant < 0.) {
            return -1;
        }
        else {
            if (discriminant > 0.) {
                double root = sqrt(discriminant);
                double sol_1 = ( -b - root )/ (2*a);
                if (sol_1 > PRECISION) {
                    return sol_1;
                }
                double sol_2 =( -b + root )/ (2*a);
                if (sol_2 > PRECISION) {
                    return sol_2;
                }
                return -1;
            }
            else {
                return -1;
            }
        }
    }

    return -1;
}

void Polynom::print() const {
    int nb = 0;
    for (int i = 0; i< degree + 1; ++i) {
        double coef = get_coef(i);
        if (coef != 0) {
            if (i==0) {
                clog << coef;
            }
            else {
                if (nb > 0) {
                    clog << " + " << coef << "*x^" << i;
                }
                else {
                    clog << coef << "*x^" << i;
                }
            }
            nb++;
        }
    }
    clog << " = 0" << endl;
}
