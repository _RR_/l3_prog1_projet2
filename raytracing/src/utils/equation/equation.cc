#include "utils/equation/equation.hh"

using namespace std;

EquationPoly::EquationPoly ( const int deg, const vector<vector<vector<double>>>& tCoeff) :
    tCoeff(tCoeff), degree(deg) {}

EquationPoly::EquationPoly ( const int deg) : degree(deg) {
    vector<double> tmp1(deg +1);
    vector<vector<double>> tmp2(deg + 1, tmp1);
    vector<vector<vector<double>>> tmp3(deg +1, tmp2);
    tCoeff = tmp3;
}

EquationPoly::EquationPoly() : EquationPoly(0) {}

EquationPoly EquationPoly::deriv ( int variable) const {
    EquationPoly res(degree);
    for (int i = 0; i < degree +1; i++) {
        for (int j = 0; j < degree +1; j++) {
            for ( int k = 0; k < degree+1; k++) {
                double coef = get_coeff(i,j,k);
                if (coef != 0) {
                    if (variable == 0 and i>0) {
                        res.tCoeff.at(i-1).at(j).at(k) = coef*i;
                    }
                    if (variable == 1 and j>0) {
                        res.tCoeff.at(i).at(j-1).at(k) = coef*j;
                    }
                    if (variable == 2 and k>0) {
                        res.tCoeff.at(i).at(j).at(k-1) = coef*k;
                    }
                }
            }
        }
    }
    return res;
}

void EquationPoly::make_grad () {
    if (gradient.size() == 0) {
        EquationPoly e_ref(degree);
        vector<EquationPoly> res(3, e_ref);
        for (int variable = 0; variable < 3; ++variable) {
            res.at(variable) = this->deriv (variable);
        }
        gradient = res;
    }
}

Vector3 EquationPoly::get_grad_value (Point3 p) {
    make_grad();
    vector<double> vals(3,0.);
    for (int i = 0; i < 3; ++i) {
        vals.at(i) = gradient.at(i).value( p );
    }
    return Vector3 ( vals.at(0), vals.at(1), vals.at(2));
}

int EquationPoly::get_degree() const {
    return degree;
}

vector<vector<vector<double>>> EquationPoly::get_tCoeff () const {
    return tCoeff;
}

double EquationPoly::get_coeff (int i, int j, int k) const {
    return tCoeff.at(i).at(j).at(k);
}

void EquationPoly::set_coef (int i, int j, int k, double coef) {
    if ((i > degree) or ( j > degree) or (k > degree )) {
    } else {
        tCoeff.at(i).at(j).at(k) = coef;
    }
}

double EquationPoly::value (const Point3 p) const {
    double val = 0.;
    double x = p.get_x();
    double y = p.get_y();
    double z = p.get_z();
    for (int i = 0; i < degree; i++) {
        for (int j = 0; j < degree; j++) {
            for ( int k = 0; k < degree; k++) {
                double coef = get_coeff(i,j,k);
                if (coef != 0) {
                    double x_i = fast_expo ( x, i);
                    double y_j = fast_expo ( y, j);
                    double z_k = fast_expo ( z, k);
                    val += coef * x_i * y_j * z_k;
                }
            }
        }
    }
    return val;
}

Polynom EquationPoly::make_polynom (const Lightray& ray) const {
    int degree_eq = get_degree();
    int degree_pol = 0;
    Point3 origin = ray.get_origin();
    Vector3 direction = ray.get_direction();

    //initialize coefficient
    double origin_x = origin.get_x();
    double origin_y = origin.get_y();
    double origin_z = origin.get_z();
    double direction_x = direction.get_x();
    double direction_y = direction.get_y();
    double direction_z = direction.get_z();

    //compute degree_pol
    for (int i = 0; i < degree_eq + 1; i++) {
        for (int j = 0; j < degree_eq + 1; j++) {
            for ( int k = 0; k < degree_eq + 1; k++) {
                if ( (i + j + k) > degree_pol and get_coeff(i, j, k) != 0.) {
                    degree_pol = i + j + k;
                }
            }
        }
    }



    //initialize the polynom induce by the ray
    Polynom ray_pol (degree_pol);

    vector<double> vector_x_ray = {origin_x, direction_x};
    vector<double> vector_y_ray = {origin_y, direction_y};
    vector<double> vector_z_ray = {origin_z, direction_z};

    Polynom pol_x_ray ( 1, vector_x_ray);
    Polynom pol_y_ray ( 1, vector_y_ray);
    Polynom pol_z_ray ( 1, vector_z_ray);

    //set coefficient of ray pol
    for (int i = 0; i < degree_eq + 1; i++) {
        for (int j = 0; j < degree_eq + 1; j++) {
            for ( int k = 0; k < degree_eq + 1; k++) {
                double coef = get_coeff(i, j, k);
                if (coef != 0.) {
                    ray_pol = ray_pol + coef * (pol_x_ray ^ i) * (pol_y_ray ^ j) * (pol_z_ray ^ k);
                }
            }
        }
    }
    return ray_pol;
}

Point3 EquationPoly::first_impact (const Lightray& ray) const {
    //org stand for origin
    Point3 org = ray.get_origin();
    //dir stand for direction
    Vector3 dir = ray.get_direction();
    double org_x = org.get_x();
    double org_y = org.get_y();
    double org_z = org.get_z();
    double dir_x = dir.get_x();
    double dir_y = dir.get_y();
    double dir_z = dir.get_z();

    //if the shape define a plane
    if (degree == 1) {
        double x = get_coeff(1,0,0);
        double y = get_coeff(0,1,0);
        double z = get_coeff(0,0,1);
        double constant = -get_coeff(0,0,0) + x*org_x + y*org_y + z*org_z;

        double coeff_dir = x*dir_x + y*dir_y + z*dir_z;
        if (coeff_dir == 0. ) {
            return org;
        }
        else {
            double t = - constant / coeff_dir;
            if (t > PRECISION) {
                return org + t*dir;
            }
            else {
                return org;
            }
        }
    }

    // if the shape define a sphere
    else {
        double x0 = -get_coeff(1,0,0)/2;
        double y0 = -get_coeff(0,1,0)/2;
        double z0 = -get_coeff(0,0,1)/2;
        double constant = get_coeff(0,0,0);
        double r2 = constant - x0*x0 - y0*y0 - z0*z0;
        double c = r2 + org_x*org_x + org_y*org_y + org_z*org_z
                   + x0*x0 + y0*y0 + z0*z0 - 2*(org_x*x0 + org_y*y0 +org_z*z0);
        double b = 2*dir_x*(org_x - x0) + 2*dir_y*(org_y - y0) + 2*dir_z*(org_z - z0);
        double a = dir_x*dir_x + dir_y*dir_y + dir_z*dir_z;
        double discriminant = b*b - 4*a*c;
        if (discriminant < 0.) {
            return org;
        }
        else {
            if (discriminant > 0.) {
                double root = sqrt(discriminant);
                double sol_1 = ( -b - root )/ (2*a);
                if (sol_1 > PRECISION) {
                    return org + (sol_1 * dir);
                }
                double sol_2 =( -b + root )/ (2*a);
                if (sol_2 > PRECISION) {
                    return org + (sol_2 * dir);
                }
                return org;
            }
            else {
                return org;
            }
        }
    }
}

Point3 EquationPoly::first_impact_general ( const Lightray& ray) const {
    Polynom p = make_polynom(ray);
    double t = p.get_first_root();
    if (t > 0.) {
        Point3 p0 = ray.get_origin() + t*(ray.get_direction() );
        return p0;
    } else {
        return ray.get_origin();
    }
}

void EquationPoly::print() const {
    int nb = 0;
    for (int i = 0; i < degree +1; i++) {
        for (int j = 0; j < degree +1; j++) {
            for ( int k = 0; k < degree +1; k++) {
                double coef = this->get_coeff(i,j,k);
                if (coef != 0) {
                    if (nb > 0) {
                        clog << " + ";
                    }
                    clog << coef;
                    if (i > 0) {
                        clog <<"x^" << i;
                    }
                    if (j > 0) {
                        clog <<"y^" << j;
                    }
                    if (k > 0) {
                        clog <<"z^" << k;
                    }
                    ++nb;
                }
            }
        }
    }
    clog << " = 0" << endl;
}

void EquationPoly::print_grad() {
    make_grad();
    for (int i = 0; i<3; i++) {
        gradient.at(i).print();
    }
}
