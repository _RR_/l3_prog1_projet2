#include "utils/color.hh"

Color Color::White(255, 255, 255);
Color Color::Black(0, 0, 0);
Color Color::Red(255, 0, 0);
Color Color::Green(0, 255, 0);
Color Color::Blue(0, 0, 255);
Color Color::Yellow(255, 255, 0);
Color Color::Magenta(255, 0, 255);
Color Color::Cyan(0, 255, 255);

Color::Color(int r, int g, int b) :
    red(r), green(g), blue(b) {
    assert(0 <= red && red <= 255);
    assert(0 <= green && green <= 255);
    assert(0 <= blue && blue <= 255);
}

int Color::get_red() const {
    return red;
}
int Color::get_green() const {
    return green;
}
int Color::get_blue() const {
    return blue;
}

string Color::get_name() const {
    return name;
}

void Color::set_name(string new_name) {
    name = new_name;
}

sf::Color Color::to_SFMLColor() const {
    return sf::Color(static_cast<sf::Uint8>(red),
                     static_cast<sf::Uint8>(green),
                     static_cast<sf::Uint8>(blue));
}

std::ostream& operator <<(std::ostream& s, const Color& c) {
    int r = c.get_red();
    int g = c.get_green();
    int b = c.get_blue();
    return s << "Color(" << r << ", " << g << ", " << b << ")";
}

Color Color::color_min(Color c1, Color c2) {
    int Red_Value = min( c1.get_red(), c2.get_red());
    int Green_Value = min( c1.get_green(), c2.get_green());
    int Blue_Value = min( c1.get_blue(), c2.get_blue());
    return Color(Red_Value, Green_Value, Blue_Value);
};

Color Color::add_colors(Color c1, Color c2) {
    int Red_Value = min(255, c1.get_red() + c2.get_red());
    int Green_Value = min(255, c1.get_green() + c2.get_green());
    int Blue_Value = min(255, c1.get_blue() + c2.get_blue());
    return Color(Red_Value, Green_Value, Blue_Value);
};

Color Color::coefficient_color(Color c1, double coef) {
    int Red_Value = coef * c1.get_red();
    int Green_Value = coef * c1.get_green();
    int Blue_Value = coef * c1.get_blue();
    return Color(Red_Value, Green_Value, Blue_Value);
}

Color Color::atenuated_color(Color c1, double coef) {
    if(coef > MAX_HUMAN_INTENSITY) {
        return c1;
    };
    double new_coef = coef / MAX_HUMAN_INTENSITY;
    return coefficient_color(c1, new_coef);
};
