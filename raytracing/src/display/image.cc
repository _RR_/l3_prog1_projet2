#include "display/image.hh"

using namespace std;

Image::Image(unsigned int width, unsigned int height) {
    image_width = width;
    image_height = height;
    // https://en.cppreference.com/w/cpp/container/vector/resize
    pixels.resize(width * height * 4);
    for (unsigned int x = 0; x < width; ++x) {
        for (unsigned int y = 0; y < height; ++y) {
            set_pixel(x, y, Color::Black); // For instance...
        }
    }
}

void Image::set_pixel(unsigned int x, unsigned int y, const Color& c) {
    // clog << "Painting pixel: " << x << " " << y << endl;
    pixels.at((x + y * image_width) * 4 + 0) = c.get_red();
    pixels.at((x + y * image_width) * 4 + 1) = c.get_green();
    pixels.at((x + y * image_width) * 4 + 2) = c.get_blue();
    pixels.at((x + y * image_width) * 4 + 3) = 255;
}

sf::Uint8* Image::get_data() {
    return(pixels.data());
}

void Image::save(const string image_path) const {
    // see: https://www.sfml-dev.org/documentation/2.0-fr/classsf_1_1Image.php
    sf::Image exported_image;
    exported_image.create(image_width, image_height, pixels.data());
    exported_image.saveToFile(image_path);
}
