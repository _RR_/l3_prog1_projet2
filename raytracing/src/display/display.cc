#include "display/display.hh"

using namespace std;

Display::Display() {
    clog << "Creating SFML Display" << endl;
}

void Display::init(unsigned width, unsigned height,
                   const string& window_title) {
    window.create(sf::VideoMode(width, height), window_title);
    window_texture.create(width, height);
    window_sprite.setTexture(window_texture);
}

void Display::set_image(Image& an_image) {
    // https://en.cppreference.com/w/cpp/container/vector/data
    window_texture.update(an_image.get_data());
    window.draw(window_sprite);
}

void Display::display() {
    window.display();
}

void Display::wait_quit_event() {
    while (window.isOpen()) {
        sf::Event event;
        // https://www.sfml-dev.org/documentation/2.5.1-fr/classsf_1_1Window.php
        // Could use the pollEvent function if needed,
        // but polling uses busy waiting, which is expensive
        while (window.waitEvent(event)) {
            switch (event.type) {
            case sf::Event::Closed:
                window.close();
                break;
            case sf::Event::KeyPressed:
                window.close();
                break;
            default:
                break;
            }
        }
    }
}
