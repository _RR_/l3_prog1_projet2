#include "world/shape/generalshape.hh"

GeneralShape::GeneralShape(const vector<double>& v, Material* material) {
    EquationPoly tmp_eq(2);
    tmp_eq.set_coef(0,0,0,v.at(0));
    tmp_eq.set_coef(1,0,0,v.at(1));
    tmp_eq.set_coef(0,1,0,v.at(2));
    tmp_eq.set_coef(0,0,1,v.at(3));
    tmp_eq.set_coef(2,0,0,v.at(4));
    tmp_eq.set_coef(0,2,0,v.at(5));
    tmp_eq.set_coef(0,0,2,v.at(6));
    tmp_eq.set_coef(1,1,0,v.at(7));
    tmp_eq.set_coef(0,1,1,v.at(8));
    tmp_eq.set_coef(1,0,1,v.at(9));
    equation = tmp_eq;
    pMaterial = material;
}

EquationPoly GeneralShape::get_equation() const {
    return equation;
}

Point3 GeneralShape::intersect (const Lightray& ray) const {
    return equation.first_impact_general(ray);
}

Vector3 GeneralShape::get_normal (const Point3& p) {
    return equation.get_grad_value(p);
}

bool GeneralShape::is_in(const Point3& p) const {
    return (equation.value(p) < PRECISION);
}

void GeneralShape::print_debug() {
    clog << "GeneralShape of material : " << pMaterial->get_name() << " and equation :";
    equation.print();
}
