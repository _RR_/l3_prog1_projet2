#include "world/shape/shape.hh"

Material* Shape::get_pMaterial() const {
    return pMaterial;
}
Shape::~Shape() {
    // Do not free material here!
    // it can be used by multiple instances leading to double free
    //free(pMaterial);
}

string Shape::get_name() const {
    return name;
}

void Shape::set_name(string new_name) {
    name = new_name;
}
