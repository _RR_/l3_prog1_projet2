#include "world/shape/mesh.hh"

// https://stackoverflow.com/questions/3599160/how-to-suppress-unused-parameter-warnings-in-c
#define UNUSED(x) (void)(x)
Mesh::Mesh(Vector3 normal, const vector<Point3>& vPoint, Material* material):
    normal(normal), vPoint(vPoint) {

    Shape::pMaterial = material;
}

Mesh::~Mesh() {
}

Point3 Mesh::intersect (const Lightray& ray) const {
    Point3 origin = ray.get_origin();
    Vector3 direction = ray.get_direction();

    Point3 p1_mesh = vPoint.at(0);
    //Point3 p2_mesh = vPoint.at(1);
    //Point3 p3_mesh = vPoint.at(2);

    Vector3 p1_o = origin - p1_mesh;
    double denominator = dot(direction,normal);
    if (denominator == 0.) {
        return origin;
    } else {
        double nominator = - dot (p1_o, normal);
        double t = nominator/denominator;
        if (t < PRECISION ) {
            return origin;
        } else {
            Point3 intersection = origin + nominator/denominator * direction;
            if (is_in (intersection)) {
                return intersection;
            }
            else {
                return origin;
            }
        }
    }
}

Vector3 Mesh::get_normal (const Point3& p) {
    UNUSED(p);
    return normal;
}

bool Mesh::is_in (const Point3& p) const {
    Point3 p1_mesh = vPoint.at(0);
    Point3 p2_mesh = vPoint.at(1);
    Point3 p3_mesh = vPoint.at(2);

    Vector3 v1 = p2_mesh - p1_mesh;
    Vector3 v2 = p3_mesh - p1_mesh;
    Vector3 v3 = p3_mesh - p2_mesh;
    Vector3 v_p1o = p - p1_mesh;
    Vector3 v_p2o = p - p2_mesh;
    Vector3 v_p3o = p - p3_mesh;

    Vector3 v1_v_p1o_cross = cross (v1, v_p1o);
    Vector3 v1_2_cross = cross (v1, v2);

    if ( dot (v1_v_p1o_cross,v1_2_cross )< 0.) {
        return false;
    }
    Vector3 v3_v_p2o_cross = cross (v3, v_p2o);
    Vector3 v1_v1_cross = cross (v3, (-1) * v1);

    if ( dot (v3_v_p2o_cross, v1_v1_cross) < 0.) {
        return false;
    }
    Vector3 v2_v_p3o_cross = cross ((-1) * v2, v_p3o);
    Vector3 v2_v3_cross = cross ((-1) * v2, (-1) * v3);

    if ( dot (v2_v_p3o_cross,v2_v3_cross) < 0.) {
        return false;
    }
    return true;
}

void Mesh::print_debug() {
    clog << "Mesh with (" << vPoint.at(0) << "," << vPoint.at(1) << ",";
    clog << vPoint.at(2) << ") with normal : " << normal << endl;

}
