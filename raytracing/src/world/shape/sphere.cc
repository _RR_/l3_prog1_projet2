#include "world/shape/sphere.hh"

Sphere::Sphere(Point3 center, double radius, Material* material) {
    EquationPoly tmp_eq(2);
    double x0 = center.get_x();
    double y0 = center.get_y();
    double z0 = center.get_z();
    double a = x0*x0 + y0*y0 + z0*z0 - radius*radius;
    tmp_eq.set_coef(0,0,0,a);
    tmp_eq.set_coef(0,0,1,-2*z0);
    tmp_eq.set_coef(0,1,0,-2*y0);
    tmp_eq.set_coef(1,0,0,-2*x0);
    tmp_eq.set_coef(0,0,2,1.);
    tmp_eq.set_coef(0,2,0,1.);
    tmp_eq.set_coef(2,0,0,1.);
    equation = tmp_eq;
    pMaterial = material;
}

Sphere::~Sphere() {
}

EquationPoly Sphere::get_equation() const {
    return equation;
}

Point3 Sphere::intersect (const Lightray& ray) const {
    return equation.first_impact(ray);
}

Vector3 Sphere::get_normal (const Point3& p) {
    return equation.get_grad_value(p);
}

bool Sphere::is_in(const Point3& p) const {
    return (equation.value(p) < PRECISION);
}

void Sphere::print_debug() {
    clog << "Sphere of material: " << pMaterial->get_name() << " & equation :";
    equation.print();
}
