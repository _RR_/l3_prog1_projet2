#include "world/shape/polyhedra.hh"

// https://stackoverflow.com/questions/3599160/how-to-suppress-unused-parameter-warnings-in-c
#define UNUSED(x) (void)(x)

Polyhedra::Polyhedra (const vector<Mesh*>& vMesh, Material* material) : vMesh(vMesh) {
    pMaterial = material;
}

Polyhedra::Polyhedra(const string path, const Vector3& origin, Material* material) {
    Shape::pMaterial = material;
    CsvLoader csv(path," ","");
    csv.open();
    csv.get_entry(); //to flush header line
    vector<string> current_data;
    Mesh* new_mesh;
    while (not csv.end_stream()) {
        csv.get_entry();// Facet_header entry
        current_data = csv.parse_entry();
        if(current_data.at(0).compare("endsolid")==0) {
            break;
        }
        vector<Point3> points;
        double n1 = stod(current_data.at(2));
        double n2 = stod(current_data.at(3));
        double n3 = stod(current_data.at(4));
        Vector3 normal(n1,n2,n3);
        csv.get_entry(); // outer loop
        csv.get_entry(); // vertex1
        current_data = csv.parse_entry();
        double x1 = stod(current_data.at(1));
        double y1 = stod(current_data.at(2));
        double z1 = stod(current_data.at(3));
        Point3 v1(x1,y1,z1);
        v1 = v1 + origin;
        points.push_back(v1);
        csv.get_entry(); // vertex2
        current_data = csv.parse_entry();
        double x2 = stod(current_data.at(1));
        double y2 = stod(current_data.at(2));
        double z2 = stod(current_data.at(3));
        Point3 v2(x2,y2,z2);
        v2 = v2 + origin;
        points.push_back(v2);
        csv.get_entry(); // vertex3
        current_data = csv.parse_entry();
        double x3 = stod(current_data.at(1));
        double y3 = stod(current_data.at(2));
        double z3 = stod(current_data.at(3));
        Point3 v3(x3,y3,z3);
        v3 = v3 + origin;
        points.push_back(v3);
        csv.get_entry(); // end loop
        csv.get_entry(); // end facet
        new_mesh = new Mesh(normal,points,material);
        vMesh.push_back(new_mesh);
    }
}

Polyhedra::~Polyhedra() {
    // free meshs
    while(not(vMesh.empty())) {
        delete vMesh.back();
        vMesh.pop_back();
    }
}

Point3 Polyhedra::intersect (const Lightray& ray) const {
    Point3 reference = ray.get_origin();
    vector<Point3> vPoint;
    //clog << "Début de test :" << endl;
    for (Mesh* mesh: vMesh) {
        Point3 intersection_point = mesh->intersect(ray);
        if (not(intersection_point == reference)) {
            vPoint.push_back(intersection_point);
        }
    }
    //clog << "Test : "<< (min_point(reference, vPoint)) << endl;
    if (vPoint.empty()) {
        return reference;
    }
    return (min_point(reference, vPoint));
}

Vector3 Polyhedra::get_normal (const Point3& p) {
    for (Mesh* mesh: vMesh) {
        if(mesh->is_in(p)) {
            return mesh->get_normal(p);
        }
    }
    return (Vector3(0,0,0));
}

bool Polyhedra::is_in (const Point3& p) const {
    for (Mesh* mesh: vMesh) {
        if (mesh->is_in(p)) {
            return true;
        }
    }
    return false;
}

void Polyhedra::print_debug() {
    clog << "Polyhedra containing : " << endl;
    for(Mesh* mesh : vMesh) {
        mesh->print_debug();
    }
    clog << "End of poly " << endl;
}
