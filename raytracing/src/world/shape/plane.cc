#include "world/shape/plane.hh"

Plane::Plane(double coef_x, double coef_y, double coef_z, double cst, Material* material) {
    EquationPoly tmp_eq(1);
    tmp_eq.set_coef(0,0,0,cst);
    tmp_eq.set_coef(1,0,0,coef_x);
    tmp_eq.set_coef(0,1,0,coef_y);
    tmp_eq.set_coef(0,0,1,coef_z);
    equation = tmp_eq;
    pMaterial = material;
}

Plane::~Plane() {
}

EquationPoly Plane::get_equation() const {
    return equation;
}

Point3 Plane::intersect (const Lightray& ray) const {
    return equation.first_impact(ray);
}

Vector3 Plane::get_normal (const Point3& p) {
    return equation.get_grad_value(p);
}

bool Plane::is_in(const Point3& p) const {
    return (equation.value(p) < PRECISION);
}

void Plane::print_debug() {
    clog << "Plane with material : " << pMaterial->get_name() << " & equation :";
    equation.print();
}
