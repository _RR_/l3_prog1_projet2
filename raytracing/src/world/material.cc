#include "world/material.hh"

Material::Material ( double reflexion, double refraction, double diffusion,
                     double transparency, Color* color) : reflexion(reflexion),
    refraction(refraction), diffusion(diffusion),
    transparency(transparency), pColor(color) {}

Material::Material () : reflexion(0.), refraction (0.),
    diffusion (0.), transparency (0.), pColor(&Color::White) {}

double Material::get_reflexion () const {
    return reflexion;
}

double Material::get_refraction () const {
    return refraction;
}

double Material::get_diffusion() const {
    return diffusion;
}

double Material::get_transparency() const {
    return transparency;
}

Color* Material::get_pColor() const {
    return pColor;
}

string Material::get_name() const {
    return name;
}

void Material::set_name(string new_name) {
    name = new_name;
}

bool Material::is_transparent () const {
    return (reflexion  + refraction)<1.;
}

bool Material::is_reflexive () const {
    return reflexion > 0.;
}

bool Material::is_diffusive () const {
    return diffusion > 0.;
}
