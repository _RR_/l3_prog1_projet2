#include "world/world.hh"

using namespace std;

World::World() {
}

vector<Shape*> World::get_vShape() {
    return vShape;
}
vector<Lightsource*> World::get_vLightsource() {
    return vLightsource;
}

vector<Camera*> World::get_vCamera() {
    return vCamera;
}

vector<Material*> World::get_vMaterial() {
    return vMaterial;
}

vector<Color*> World::get_vColor() {
    return vColor;
}

void World::add(Shape* ashape) {
    vShape.push_back(ashape);
}

void World::add(Lightsource* als) {
    vLightsource.push_back(als);
}

void World::add(Camera* acamera) {
    vCamera.push_back(acamera);
}

void World::add(Material* amaterial) {
    vMaterial.push_back(amaterial);
}

void World::add(Color* acolor) {
    vColor.push_back(acolor);
}

void World::from_csv(string file_name) {
    /* open csv and parse it */
    clog << "Loading world from " << file_name << endl << endl;
    CsvLoader csv(file_name, string(","),string(" "));
    vector<string> parsed;
    if(csv.open()) {

        while(not(csv.end_stream())) {
            csv.get_entry();
            parsed = csv.parse_entry();
            World::instanciate_object(parsed);
        }
    }
}

void World::instanciate_object(vector<string> parsed) {
    /* if there is things to parse check if if it is one of the reserved key
     * and call the according function to instanciate the object */
    if(parsed.size() > 1) {
        if(parsed.at(0).compare("sphere") == 0)
        {
            //instanciate sphere and add it to the world
            World::instanciate_sphere(parsed);
        }
        if(parsed.at(0).compare("plane") == 0)
        {
            //instanciate plane and add it to the world
            World::instanciate_plane(parsed);
        }
        if(parsed.at(0).compare("polyhedra") == 0)
        {
            //instanciate polyhedra and add it to the world
            World::instanciate_polyhedra(parsed);
        }
        if(parsed.at(0).compare("generalshape") == 0)
        {
            //instanciate polyhedra and add it to the world
            World::instanciate_general_shape(parsed);
        }
        if(parsed.at(0).compare("material") == 0)
        {
            //instanciate material and add it to the world
            World::instanciate_material(parsed);
        }
        if(parsed.at(0).compare("lightsource") == 0)
        {
            //instanciate lightsource and add it to the world
            World::instanciate_ligthsource(parsed);
        }
        if(parsed.at(0).compare("color") == 0)
        {
            //instanciate color and add it to the world
            World::instanciate_color(parsed);
        }
    }

}

void World::instanciate_material(vector<string> parsed) {
    assert(parsed.size() >= 7);

    string material_name = parsed.at(1);
    double reflexion = stod(parsed.at(2));
    double refraction = stod(parsed.at(3));
    double diffusion = stod(parsed.at(4));
    double transparency = stod(parsed.at(5));
    string color_name = parsed.at(6);

    clog << "instanciating material:" << material_name << ":"
         << reflexion << " " << refraction << " "
         << diffusion << " " << transparency << " Color="
         << color_name << endl << endl;

    Material* mat = new Material(reflexion, refraction, diffusion, transparency, World::get_color(color_name));
    mat->set_name(material_name);
    World::add(mat);
}

Material* World::get_material(string name) {
    for(Material* mat: vMaterial) {
        if(name.compare(mat->get_name()) == 0) {
            return mat;
        }
    }
    clog << "Material not found (Error in the way)" << endl;
    return NULL;
}

void World::instanciate_color(vector<string> parsed) {
    assert(parsed.size() >= 5);

    string color_name = parsed.at(1);
    int r = stoi(parsed.at(2));
    int g = stoi(parsed.at(3));
    int b = stoi(parsed.at(4));

    clog << "instanciating color:" << color_name << ":"
         << r << ", " << g << ", " << b << endl << endl;

    Color* col = new Color(r,g,b);
    col->set_name(color_name);
    World::add(col);
}

Color* get_color(string name);
Color* World::get_color(string name) {
    for(Color* col: vColor) {
        if(name.compare(col->get_name()) == 0) {
            return col;
        }
    }
    clog << "Color not found (Error in the way)" << endl;
    return NULL;
}
void World::instanciate_sphere(vector<string> parsed) {
    assert(parsed.size() >= 7);

    string shape_name = parsed.at(1);
    double x = stod(parsed.at(2));
    double y = stod(parsed.at(3));
    double z = stod(parsed.at(4));
    double diameter = stod(parsed.at(5));
    string material_name = parsed.at(6);

    Sphere* s =  (new Sphere(Point3(x,y,z), diameter, World::get_material(material_name)));
    s->set_name(shape_name);
    World::add(s);
}

void World::instanciate_plane(vector<string> parsed) {
    assert(parsed.size() >= 7);

    string shape_name = parsed.at(1);
    double x = stod(parsed.at(2));
    double y = stod(parsed.at(3));
    double z = stod(parsed.at(4));
    double c = stod(parsed.at(5));
    string material_name = parsed.at(6);

    Plane* p =  new Plane(x, y, z, c, World::get_material(material_name));
    p->set_name(shape_name);
    World::add(p);
}

void World::instanciate_polyhedra(vector<string> parsed) {
    assert(parsed.size() >= 7);

    string shape_name = parsed.at(1);
    string poly_path = parsed.at(2);
    double x = stod(parsed.at(3));
    double y = stod(parsed.at(4));
    double z = stod(parsed.at(5));
    string material_name = parsed.at(6);

    Polyhedra* p =  new Polyhedra(poly_path, Vector3(x,y,z), World::get_material(material_name));
    p->set_name(shape_name);
    World::add(p);
}

void World::instanciate_general_shape(vector<string> parsed) {
    assert(parsed.size() >= 5);

    string shape_name = parsed.at(1);
    unsigned int vector_size = stoi(parsed.at(2));
    vector<double> v(vector_size);
    for(unsigned int i=0; i<vector_size; ++i) {
        v.at(i) = stod(parsed.at(i+3));
    }
    string material_name = parsed.at(vector_size+3);

    GeneralShape* p = new GeneralShape (v,World::get_material(material_name));
    p->set_name(shape_name);
    World::add(p);
}

void World::instanciate_ligthsource(vector<string> parsed) {
    assert(parsed.size() >= 7);

    string lightsource_name = parsed.at(1);
    double x = stod(parsed.at(2));
    double y = stod(parsed.at(3));
    double z = stod(parsed.at(4));
    double intensity = stod(parsed.at(5));
    string color_name = parsed.at(6);
    Lightsource* ls = new Lightsource(Point3(x, y, z), intensity, World::get_color(color_name));
    ls->set_name(lightsource_name);
    World::add(ls);
}

void World::free() {
    // free shapes
    while(not(vShape.empty())) {
        //free shape
        delete vShape.back();
        // remove it from the table
        vShape.pop_back();
    }
    // free ligthsources
    while(not(vLightsource.empty())) {
        delete vLightsource.back();
        vLightsource.pop_back();
    }
    // free camera
    while(not(vCamera.empty())) {
        delete vCamera.back();
        vCamera.pop_back();
    }
    // free matrials
    while(not(vMaterial.empty())) {
        delete vMaterial.back();
        vMaterial.pop_back();
    }
    // free colors 
    while(not(vColor.empty())) {
        delete vColor.back();
        vColor.pop_back();
    }
}

void World::print_debug() {
    for(Shape* s : vShape) {
        s -> print_debug();
        Material* m = s -> get_pMaterial();
        Color* c = m -> get_pColor();
        clog << "Color (" << c->get_red() << "," << c->get_green() << "," << c->get_blue() << ")" << endl;
    }
    for(Lightsource* l : vLightsource) {
        l -> print_debug();
    }
    for(Material* m : vMaterial) {
        clog << "Material : "<< (m -> get_name()) << endl;
    }
}
