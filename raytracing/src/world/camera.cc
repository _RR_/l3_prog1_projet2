#include "world/camera.hh"

// https://stackoverflow.com/questions/3599160/how-to-suppress-unused-parameter-warnings-in-c
#define UNUSED(x) (void)(x)
using namespace std;

Camera::Camera(double x_max, double y_max, Point3 position) :
    x_max(x_max), y_max(y_max), position(position) {};

Lightray Camera::get_ray(double x, double y) const {
    UNUSED(x_max);
    UNUSED(y_max);
    return Lightray(position, Point3(x,y,0) - position);
}


string Camera::get_name() const {
    return name;
}

void Camera::set_name(string new_name) {
    name = new_name;
}
