#include "world/lightsource.hh"

/* Constructors */
Lightsource::Lightsource(Point3 position, double intensity, Color* color) :
    position(position), intensity(intensity), pColor(color) {}

/* Getters */
Point3 Lightsource::get_position() const {
    return position;
}

double Lightsource::get_intensity() const {
    return intensity;
}

Color* Lightsource::get_pColor() const {
    return pColor;
}

/* Others */
Point3 Lightsource::intersect (Lightray ray) const {
    return ray.get_origin();
}

string Lightsource::get_name() const {
    return name;
}

void Lightsource::set_name(string new_name) {
    name = new_name;
}

void Lightsource::print_debug() {
    clog << "Lightsource : Position = " << position << ", intensity = "<< intensity << endl;
}
