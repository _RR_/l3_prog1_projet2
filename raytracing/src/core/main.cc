#include <iostream>
#include <exception>
#include <cassert>
#include <cmath>
#include <iomanip>
#include <vector>
#include <cfloat>

#include <utility>

#include "utils/vector3.hh"
#include "utils/point3.hh"
#include "utils/color.hh"
#include "display/image.hh"
#include "display/display.hh"
#include "world/shape/shape.hh"
#include "world/shape/sphere.hh"
#include "world/shape/plane.hh"
#include "world/shape/mesh.hh"
#include "world/shape/polyhedra.hh"
#include "world/shape/generalshape.hh"
#include "world/camera.hh"
#include "world/lightsource.hh"
#include "world/world.hh"
#include "render/raytracer.hh"

using namespace std;

int main(int argc, char* argv[]) {
    // Thanks
    // https://stackoverflow.com/questions/39161002/how-to-disable-stdclog-logging-from-source-code
    // Disable clog
    std::clog.setstate(std::ios_base::failbit);

    // define picture size
    const unsigned width = 1200;
    const unsigned height = 800;
    // define camera
    double position_x_screen = -(static_cast<double>(width))/2;
    double position_y_screen = -(static_cast<double>(height))/2;
    Camera* camera1 = new Camera((double) width, (double) height, Point3(-position_x_screen,-position_y_screen,-500.0));


    // set default
    string file_to_load = "models/world_default.csv";
    RayTracer::RenderType render_type = RayTracer::RECURSIVE;
    bool there_is_args_to_handle = false;
    bool use_display = true;
    bool output_image = false;
    string image_path;
    // handle args
    switch(argc) {
    case 0:
    case 1:
        file_to_load = "models/world_default.csv";
        break;
    default:
        there_is_args_to_handle = true;
    }
    // if there are args to handle
    if(there_is_args_to_handle) {
        for(int i=1; i<argc; i++) {
            std::string arg = argv[i];

            if(arg == "-h") {
                cout << "displaying help:" << endl <<
                     "Call: " << argv[0] << " <parameters>" << endl <<
                     "With parameters being:" << endl <<
                     " -i <file_world_csv_to_load> load world from specified csv formated file" << endl <<
                     " -d  disable windows vizualisation" << endl <<
                     " -h  display this help and quit" << endl <<
                     " -o <image_path.extension> save rendered image to <image_path.extension>" << endl <<
                     " -r <number> choose the render to be used (0 for back and white / 1 for simple color / 2 for diffusion only render / 3 for the full recursive render)" << endl <<
                     " -v  enable log output (suppressed by defaut)" << endl;
                return(0);
            }
            else if(arg == "-v") {
                // reenabling clog
                std::clog.clear();
            }
            else if(arg == "-i") {
                file_to_load = argv[i+1];
            }
            else if(arg == "-r") {
                std::string arg_next = argv[i+1];
                if(arg_next == "0") {
                    render_type = RayTracer::BLACK_AND_WHITE;
                }
                if(arg_next == "1") {
                    render_type = RayTracer::SIMPLE_COLOR;
                }
                if(arg_next == "2") {
                    render_type = RayTracer::DIFFUSION_ONLY;
                }
                if(arg_next == "3") {
                    render_type = RayTracer::RECURSIVE;
                }
            }
            else if(arg == "-o") {
                image_path = argv[i+1];
                output_image = true;
            }
            else if(arg == "-d") {
                use_display = false;
            }
        }
    }


    World myWorld;
    myWorld.from_csv(file_to_load); // and load it for CSV

    myWorld.add(camera1);
    myWorld.print_debug();

    // initialise render
    pair<unsigned int, unsigned int> screen_size(width, height);
    RayTracer myrender(myWorld, screen_size, camera1, 3);

    // compute render
    Image render_result(width, height);
    clog << "Starting render" << endl;
    myrender.compute(render_type, render_result);
    // render done freeing memory
    myWorld.free();

    // save image if needed
    if(output_image) {
        render_result.save(image_path);
    }

    // display image if needed
    if(use_display) {
        Display display;
        display.init(width, height, "The World");
        display.set_image(render_result);
        display.display();
        display.wait_quit_event();
    }
    return 0;
}
