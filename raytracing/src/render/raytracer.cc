#include "render/raytracer.hh"

using namespace std;

RayTracer::RayTracer(World& aworld, const pair<unsigned int, unsigned int>&  an_image_size, Camera* camera_used, const int max_reflexion) :
    world(aworld), image_size(an_image_size), camera(camera_used), max_reflexion(max_reflexion) {

}

void RayTracer::compute(RayTracer::RenderType render_type, Image& image) {
    Color c;
    // ray gun
    for(unsigned int x=0; x<image_size.first; ++x) {
        for(unsigned int y=0; y<image_size.second; ++y) {
            c = RayTracer::trace(render_type, x, y);
            image.set_pixel(x, y, c);
        }
    }
}



Color RayTracer::trace(RayTracer::RenderType render_type, const unsigned int x, const unsigned int y)
{
    Lightray first_ray = camera->get_ray((double) x, (double) y);
    vector<Hit> vHit(max_reflexion);
    enum RenderType {BLACK_AND_WHITE, SIMPLE_COLOR, DIFFUSION_ONLY, RECURSIVE};
    switch(render_type) {
    case RayTracer::BLACK_AND_WHITE:
        return RayTracer::dumb_trace(first_ray);
        break;
    case RayTracer::SIMPLE_COLOR:
        return RayTracer::lesser_dumb_trace(first_ray);
        break;
    case RayTracer::DIFFUSION_ONLY:
        return RayTracer::way_lesser_dumb_trace(first_ray);
        break;
    case RayTracer::RECURSIVE:
        return RayTracer::rectrace(first_ray, max_reflexion);
        break;
    default:
        return RayTracer::way_lesser_dumb_trace(first_ray);
    }
}

Color RayTracer::dumb_trace(Lightray& ray) {
    // collect potential shape
    vector<Hit> vHit = compute_hitted_shape(world.get_vShape(), ray);
    // No valid hit
    if(vHit.empty()) {
        // No light in sight ;\ ->back into the darkness
        return Color::Black;
    }
    // valid hit
    return Color::White;
}

Color RayTracer::lesser_dumb_trace(Lightray& ray) {
    // collect potential shape
    vector<Hit> vHit = compute_hitted_shape(world.get_vShape(), ray);
    // No valid hit
    if(vHit.empty()) {
        // No light in sight ;\ ->back into the darkness
        return Color::Black;
    }
    // valid hit
    Hit real_hit = min_hit(ray.get_origin(), vHit);
    return *(real_hit.get_pShape()->get_pMaterial()->get_pColor());
}

Color RayTracer::way_lesser_dumb_trace(Lightray& ray) {
    // collect potential shape
    vector<Hit> vHit = compute_hitted_shape(world.get_vShape(), ray);
    // No valid hit
    if(vHit.empty()) {
        // No light in sight ;\ ->back into the darkness
        return Color::Black;
    }
    // valid hit
    Hit real_hit = min_hit(ray.get_origin(), vHit);
    Color c = compute_diffusion(real_hit);
    return c;
}

Color RayTracer::compute_diffusion(Hit& hit) {
    vector<Lightsource*> vLightsource = world.get_vLightsource();
    return compute_color(world.get_vShape(),vLightsource, hit);
}


Color RayTracer::rectrace(Lightray& ray, const unsigned int count) {
    // if max reflexion limit is here
    if(count <= 0)
    {
        return Color::Black;
    }

    vector<Hit> vHit = compute_hitted_shape(world.get_vShape(), ray);
    // check if light collided with something
    if(vHit.empty()) {
        // No light in sight ;\ ->back into the darkness
        return Color::Black;
    }
    // compute min hit points by distance from the source
    //Hit* first_hit = Utils::min_hit(ray.get_origin(), vHit);
    //return Color::White;

    Hit real_hit = min_hit(ray.get_origin(), vHit);
    // get proprieties
    Shape* pShape = real_hit.get_pShape();
    Material* pMaterial = pShape->get_pMaterial();
    Color new_color = Color::Black;

    // are we entering the shape
    if(dot(pShape->get_normal(real_hit.get_position()), ray.get_direction()) != 0) {
        // define base color

        // compute diffusion if needed
        if(pMaterial->is_diffusive()) {
            Color diffusion = compute_diffusion(real_hit);
            new_color = Color::add_colors(new_color, Color::coefficient_color(diffusion, pMaterial->get_diffusion()));
        }

        // Compute reflexion if needed
        if(pMaterial->is_reflexive()) {
            // Compute the symetric
            Vector3 direction = -ray.get_direction();
            Vector3 norm = normalize(real_hit.get_normal());
            Vector3 projection = dot(direction, norm) * norm;
            Vector3 ortho = direction - projection;
            Vector3 new_direction = direction - (2 * ortho);
            // new light ray
            Lightray new_ray(real_hit.get_position(), new_direction);
            Color reflexion = rectrace(new_ray,count-1);
            new_color = Color::add_colors(new_color, Color::coefficient_color(reflexion, pMaterial->get_reflexion()));
        }

        // Compute transparency if needed
        if(pMaterial->is_transparent()) {
            Lightray new_ray(real_hit.get_position(), ray.get_direction());
            Color transparency = rectrace(new_ray, count-1);
            new_color = Color::add_colors(new_color, Color::coefficient_color(transparency, pMaterial->get_transparency()));
        }
    }
    else {
        // we are exiting (only if transparent)
        if(pMaterial->is_transparent()) {
            Color transparency = Color::Black;
            Lightray new_ray(real_hit.get_position(), ray.get_direction());
            new_color = rectrace(new_ray, count-1);
        }
    }
    return new_color;

}
