#include "render/raytracer_utils.hh"

using namespace std;

Color diffuse_with_transparency(const vector<Hit> hits, Lightsource light) {
    /*** Returns the light color taking into account the transparent materials
         it went through    ***/
    Color c = *light.get_pColor();
    for(Hit hit : hits) {
        Shape* shape_v = hit.get_pShape();
        Material* material_v = shape_v->get_pMaterial();
        Color color_v = *material_v->get_pColor();
        double coef = material_v->get_transparency();
        c = Color::color_min(c, color_v);
        c = Color::coefficient_color(c, coef);
    }
    return c;
};

vector<Hit> compute_hitted_shape(const vector<Shape*> vShape, const Lightray& ray) {
    vector<Hit> vHit;
    // collect potential shape
    for(Shape* shape: vShape) {
        Hit hitused = Hit(shape, ray);
        if(hitused.is_hit()){
            // only store if it is a valid hit
            vHit.push_back(hitused);
        }
    }
    return vHit;
}

Hit min_hit(Point3 reference, vector<Hit>& vHit) {
    Hit minhit = vHit.at(0);
    for(Hit hit: vHit) {
        if(compare_vect(hit.get_position() - reference, minhit.get_position() - reference)){
            minhit = hit;
        }
    }
    return minhit;
}


bool is_a_hit_nearer_than(const vector<Hit> vHit, Point3 origin, double min_dist2) {
    Vector3 vect_to_hit;
    for(Hit hit: vHit) {
        vect_to_hit = hit.get_position() - origin;
        if(dot(vect_to_hit, vect_to_hit) < min_dist2) {
            return true;
        }
    }
    return false;
}

vector<Hit> all_hits_nearer_than(const vector<Hit> vHit, Point3 origin, double min_dist2) {
    Vector3 vect_to_hit;
    vector<Hit> hitted;
    for(Hit hit: vHit) {
        vect_to_hit = hit.get_position() - origin;
        if(dot(vect_to_hit, vect_to_hit) < min_dist2) {
            hitted.push_back(hit);
        }
    }
    return hitted;
}

Color compute_color( const vector<Shape*> vShape, const vector<Lightsource*> lights, const Hit& hit_point) {

    //default color is black
    Color color(0,0,0);
    Color material_color = *(hit_point.get_pShape()->get_pMaterial()->get_pColor());
    for(Lightsource* l : lights) {
        // we compute all data we need to have coefficients
        Vector3 vect_to_light = l->get_position() - hit_point.get_position();
        Lightray lr(hit_point.get_position(),vect_to_light);
        double distance = dot(vect_to_light, vect_to_light);
        vector<Hit> all_hits = compute_hitted_shape(vShape,lr);
        vector<Hit> hits = all_hits_nearer_than(all_hits, hit_point.get_position(), distance);
        // we compute the light color with transparent object on the way
        Color light_color = diffuse_with_transparency(hits,*l);
        double distance_coefficient = 1;
        // we compute the distance coefficient
        if(dot(vect_to_light, vect_to_light) > PRECISION) {
            /* the coefficient is on a 1/r²*(sphere surface) scale with
            arbitrary constants */
            distance_coefficient = min(1.,1000000 / ( distance * 4 * 3.14159265358979323846));
        }
        // we compute the angle coefficient
        double angle_coefficient = get_cos_angle( vect_to_light, hit_point.get_normal());
        double intensity = l->get_intensity() * angle_coefficient * distance_coefficient;
        Color ideal_light_color = Color::atenuated_color( light_color, intensity);
        // Substractive synthesis of colors
        Color mixed_color = Color::color_min(material_color, ideal_light_color);
        // new color with this lightsource and its coefficients
        color = Color::add_colors(color, mixed_color);
    }
    return color;
};
