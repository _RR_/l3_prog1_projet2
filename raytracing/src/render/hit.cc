#include "render/hit.hh"

/* Constructors */

Hit::Hit(Shape* shape, const Lightray& ray) : ray(ray) {
    pShape = shape;
    // compute normal if possible
    position = shape->intersect(ray);
    if (position == ray.get_origin()) {
        normal = Vector3(0,0,0);
    }
    else {
        normal = shape->get_normal(position);
    }
}

/* Getters */

Point3 Hit::get_position() const {
    return position;
}

Vector3 Hit::get_normal() const {
    return normal;
}

Shape* Hit::get_pShape() const {
    return pShape;
}

bool Hit::is_hit() const {
    // is there a hit (intersection tell you that in the initialization)
    return (!(normal == (Vector3(0,0,0))));
}

void Hit::debug() const {
    clog << "x :" << position.get_x() << "  y :" << position.get_y() << "  z :" << position.get_z() << endl;
}

Hit::~Hit() {
    //free(pShape);
}
