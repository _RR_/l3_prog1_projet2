#include "render/lightray.hh"

/* Constructors */

Lightray::Lightray(Point3 origin, Vector3 direction) :
    origin(origin), direction(direction) {
}

/* Getters */
Point3 Lightray::get_origin() const {
    return origin;
}

Vector3 Lightray::get_direction() const {
    return direction;
}
