#ifndef IMAGE_HH
#define IMAGE_HH


#include <SFML/Graphics.hpp>
#include "utils/color.hh"

using namespace std;

class Image {
public:
    Image(unsigned width, unsigned height);
    void set_pixel(unsigned int x, unsigned int y, const Color& c);
    sf::Uint8* get_data();
    void save(const string image_path) const;

private:
    unsigned int image_width;
    unsigned int image_height;
    std::vector<sf::Uint8> pixels;
};


#endif /* end of include guard: IMAGE_HH */
