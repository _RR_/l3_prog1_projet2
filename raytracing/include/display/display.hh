#ifndef DISPLAY_HH
#define DISPLAY_HH

#include <string>
#include <iostream>
#include <SFML/Graphics.hpp>
#include "display/image.hh"

class Display {
public:
    Display();
    void init(unsigned width, unsigned height,
              const std::string& window_title);
    void set_image(Image& an_image);
    void display();
    void wait_quit_event();

private:
    sf::RenderWindow window;
    //unsigned window_width;
    sf::Texture window_texture;
    sf::Sprite window_sprite;
};


#endif /* end of include guard: DISPLAY_HH */
