#ifndef LIGTHRAY_HH
#define LIGTHRAY_HH

#include "utils/vector3.hh"
#include "utils/point3.hh"
#include "utils/color.hh"

using namespace std;

class Lightray {

private:
    Point3 origin;
    Vector3 direction;

public:
    /* Constructors */
    Lightray() = default;
    Lightray(Point3 origin, Vector3 direction);

    /* Getters */
    Point3 get_origin() const;
    Vector3 get_direction() const;

};

#endif /* end of include guard: LIGTHRAY_HH */
