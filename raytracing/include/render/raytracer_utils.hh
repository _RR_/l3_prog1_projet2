#ifndef RAYTRACER_UTILS_HH
#define RAYTRACER_UTILS_HH

#include <cmath>
#include "render/lightray.hh"
#include "render/hit.hh"
#include "world/lightsource.hh"
#include "utils/color.hh"
#include "utils/utils.hh"
#include "utils/const.hh"

using namespace std;

Color compute_color( const vector<Shape*> vShape, vector<Lightsource*> lights, const Hit& hit_point);
vector<Hit> compute_hitted_shape(const vector<Shape*> vShape, const Lightray& ray);
Hit min_hit(Point3 reference, vector<Hit>& vHit);
bool is_a_hit_nearer_than(const vector<Hit> vHit, Point3 origin, double min_dist2);

#endif /* end of include guard: RAYTRACER_UTILS_HH */
