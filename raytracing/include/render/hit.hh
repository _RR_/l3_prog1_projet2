#ifndef HIT_HH
#define HIT_HH

#include <iostream>
#include "utils/vector3.hh"
#include "utils/point3.hh"
#include "world/shape/shape.hh"
#include "render/lightray.hh"

using namespace std;

class Shape;
class Lightsource;

class Hit {

private:
    Point3 position;
    Vector3 normal;
    Shape* pShape = NULL;
    Lightray ray;

public:
    Hit() = default;
    Hit(Shape* pShape, const Lightray& ray);

    /* Getters */
    Point3 get_position() const;
    Vector3 get_normal() const;
    Shape* get_pShape() const;
    bool is_hit() const;
    void debug() const;

    /* Destructors */
    ~Hit();
};

#endif /* end of include guard: HIT_HH */
