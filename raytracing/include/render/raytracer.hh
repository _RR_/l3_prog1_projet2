#ifndef RAYTRACER_HH
#define RAYTRACER_HH

#include "world/camera.hh"
#include "world/world.hh"
#include "render/lightray.hh"
#include "render/hit.hh"
#include "display/image.hh"
#include "render/raytracer_utils.hh"
#include "utils/vector3.hh"
#include "utils/color.hh"

using namespace std;
//typedef pair<Shape*, Hit> shapeAndHit;

class RayTracer {
private :
    //int state;
    World world;
    const pair<unsigned int, unsigned int> image_size;
    Camera* camera;
    const int max_reflexion;
public :
    enum RenderType {BLACK_AND_WHITE, SIMPLE_COLOR, DIFFUSION_ONLY, RECURSIVE};
    RayTracer(World& aworld, const pair<unsigned int, unsigned int>& an_image_size, Camera* camera, const int max_reflexion);
    void compute(RenderType render_type, Image& image);
    Color trace(RenderType render_type, const unsigned int x, const unsigned int y);
    Color dumb_trace(Lightray& ray);
    Color lesser_dumb_trace(Lightray& ray);
    Color way_lesser_dumb_trace(Lightray& ray);
    Color rectrace(Lightray& ray, const unsigned int count);
    Color compute_diffusion(Hit& hit);
};

#endif
