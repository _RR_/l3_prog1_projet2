#ifndef CSVLOADER_HH
#define CSVLOADER_HH

#include <vector>
#include <string>
#include <fstream> // for file stream
#include <algorithm>
#include <iostream>


using namespace std;

// Thanks to
// http://www.cplusplus.com/reference/string/string/
// for all the string functions
class CsvLoader {
private:
    const string file_path;
    const string separator;
    const string ignored_char;
    string entry;
    ifstream in;
public:
    CsvLoader(string data, string separator, string ignored_char);
    bool open();
    bool end_stream();
    string get_entry();
    vector<string> parse_entry();
    string delete_ignored(string line);
};

#endif
