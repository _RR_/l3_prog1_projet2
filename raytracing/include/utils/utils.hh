#ifndef UTILS_HH
#define UTILS_HH

#include "utils/point3.hh"

using namespace std;

double fast_expo(double x, int n);
double get_cos_angle (const Vector3& v1, const Vector3& v2);

#endif
