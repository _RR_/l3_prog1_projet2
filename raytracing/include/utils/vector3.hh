#ifndef VECTOR3_HH
#define VECTOR3_HH

#include <iostream>
#include <cmath>
#include <cassert>


class Vector3 {
public:

    Vector3() = default;
    Vector3(double x, double y, double z);
    Vector3(const Vector3& other) = default;
    ~Vector3() = default;
    Vector3& operator =(const Vector3& rhs) = default;

    double length() const;

    double get_x() const;
    double get_y() const;
    double get_z() const;

private:

    double x;
    double y;
    double z;
};

Vector3 operator *(double t, const Vector3& v);
Vector3 operator /(const Vector3& v, double t);
Vector3 operator +(const Vector3& u, const Vector3& v);
Vector3 operator -(const Vector3& u, const Vector3& v);
Vector3 operator -(const Vector3& v);

bool operator ==(const Vector3& v1, const Vector3& v2);
bool is(const Vector3& v1, const Vector3& v2, double precision);

bool compare_vect(const Vector3 v1, const Vector3 v2);

double dot(const Vector3& v1, const Vector3& v2);
Vector3 cross(const Vector3& v1, const Vector3& v2);

Vector3 normalize(Vector3 v);

std::ostream& operator <<(std::ostream& s, const Vector3& v);



#endif /* end of include guard: VECTOR3_HH */
