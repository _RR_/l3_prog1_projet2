#ifndef POLYNOM_HH
#define POLYNOM_HH

#include <vector>
#include <iostream>
#include <cmath>
#include "utils/const.hh"
#include "utils/utils.hh"
#include "utils/point3.hh"

using namespace std;

class Polynom {

private :
    int degree;
    vector<double> vCoeff;

public :
    Polynom(const int deg, const vector<double>& vCoeff);
    Polynom(const int deg);
    Polynom();
    double get_coef (int i) const;
    Polynom operator * (const Polynom& p) const;
    Polynom operator + (const Polynom& p) const;
    int get_degree () const;
    vector<double> get_vCoeff() const;
    double get_first_root() const;
    void print() const;
};

Polynom operator * (double lambda, const Polynom& p);
Polynom operator ^ (const Polynom& p, int n);

#endif /* end of include guard: POLYNOM_HH */
