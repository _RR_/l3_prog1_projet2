#ifndef EQUATION_HH
#define EQUATION_HH

#include <vector>
#include "utils/const.hh"
#include "utils/point3.hh"
#include "utils/vector3.hh"
#include "utils/equation/polynom.hh"
#include "render/lightray.hh"
#include "utils/const.hh"

class EquationPoly {
private :
    vector<vector<vector<double>>> tCoeff;
    int degree;
    vector<EquationPoly> gradient;
public :
    EquationPoly (const int deg, const vector<vector<vector<double>>>& tCoeff);
    EquationPoly (const int deg);
    EquationPoly ();
    EquationPoly deriv (int variable) const;
    void make_grad ();
    Vector3 get_grad_value (Point3 p);
    int get_degree () const;
    vector<vector<vector<double>>> get_tCoeff () const;
    double get_coeff (int i, int j, int k) const;
    void set_coef (int i, int j, int k, double coef);
    double value (const Point3 p) const;
    Polynom make_polynom (const Lightray& r) const;
    Point3 first_impact (const Lightray& ray) const;
    Point3 first_impact_general ( const Lightray& ray) const;
    void print_grad();
    void print () const;
};

#endif
