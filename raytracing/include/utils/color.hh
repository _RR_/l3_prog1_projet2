/* Copyright (c) 2017-2018 Jean-Michel Gorius */
#ifndef COLOR_HH
#define COLOR_HH

#include <SFML/Graphics.hpp>
#include <string>
#include <cassert>
#include "utils/const.hh"

using namespace std;

class Color {
public:

    static Color White;
    static Color Black;
    static Color Red;
    static Color Green;
    static Color Blue;
    static Color Yellow;
    static Color Magenta;
    static Color Cyan;

    static Color color_min(Color c1, Color c2);
    static Color add_colors(Color c1, Color c2);
    static Color coefficient_color(Color c1, double coef);
    static Color atenuated_color(Color c1, double coef);

    Color() = default;
    Color(int red, int green, int blue);
    Color(const Color& other) = default;
    Color& operator =(const Color& rhs) = default;

    sf::Color to_SFMLColor() const;

    int get_red() const;
    int get_green() const;
    int get_blue() const;
    string get_name() const;
    void set_name(string new_name);


private:
    int red;
    int green;
    int blue;
    string name = "default";
};

std::ostream& operator <<(std::ostream& s, const Color& c);


#endif /* end of include guard: COLOR_HH */
