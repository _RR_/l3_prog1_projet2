#ifndef CAMERA_HH
#define CAMERA_HH

#include <string>
#include "render/lightray.hh"
#include "utils/point3.hh"

using namespace std;

class Camera {

private:
    double x_max;
    double y_max;
    Point3 position;
    string name = "noname";

public:
    /* Constructor */
    Camera(double x_max, double y_max, Point3 position);

    /* Getters */
    Lightray get_ray(double x, double y) const;

    string get_name() const;
    void set_name(string new_name);

};





#endif /* end of include guard: CAMERA_HH */
