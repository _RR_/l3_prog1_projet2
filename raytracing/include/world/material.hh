#ifndef MATERIAL_HH
#define MATERIAL_HH

#include<string>
#include "utils/color.hh"

using namespace std;

class Material {
private :
    double reflexion;
    double refraction;
    double diffusion;
    double transparency;
    Color* pColor;
    string name = "noname";
public :
    Material ();
    Material ( double reflexion, double refraction, double diffusion, double transparency, Color* color);
    double get_reflexion () const;
    double get_refraction () const;
    double get_diffusion () const;
    double get_transparency () const;
    bool is_transparent() const;
    bool is_diffusive() const;
    bool is_reflexive() const;
    Color* get_pColor() const;

    string get_name() const;
    void set_name(string new_name);
};



#endif
