#ifndef WORLD_HH
#define WORLD_HH

#include <vector>
#include <string>
#include <cassert>
#include "utils/color.hh"
#include "utils/csvloader.hh"
#include "world/shape/shape.hh"
#include "world/shape/sphere.hh"
#include "world/shape/plane.hh"
#include "world/shape/polyhedra.hh"
#include "world/shape/generalshape.hh"
#include "world/lightsource.hh"
#include "world/camera.hh"
#include "world/material.hh"

using namespace std;

class World {

private:
    vector<Shape*> vShape;
    vector<Lightsource*> vLightsource;
    vector<Camera*> vCamera;
    vector<Material*> vMaterial;
    vector<Color*> vColor;

public:

    /* Constructors */
    World();

    /* Getters */
    vector<Shape*> get_vShape();
    vector<Lightsource*> get_vLightsource();
    vector<Camera*> get_vCamera();
    vector<Material*> get_vMaterial();
    vector<Color*> get_vColor();

    /* Setters */
    void add(Shape* ashape);
    void add(Lightsource* als);
    void add(Camera* acamera);
    void add(Material* amaterial);
    void add(Color* acolor);
    // load world from csv file
    void from_csv(string file_name);
    // helper function to instanciate world
    void instanciate_object(vector<string> parsed);
    void instanciate_material(vector<string> parsed);
    Material* get_material(string name);
    void instanciate_color(vector<string> parsed);
    Color* get_color(string name);
    void instanciate_sphere(vector<string> parsed);
    void instanciate_plane(vector<string> parsed);
    void instanciate_polyhedra(vector<string> parsed);
    void instanciate_general_shape(vector<string> parsed);
    void instanciate_ligthsource(vector<string> parsed);
    // free memory
    void free();

    /* Debug */
    void print_debug();
};

#endif /* end of include guard: WORLD_HH */
