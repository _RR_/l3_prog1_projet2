#ifndef LIGHTSOURCE_HH
#define LIGHTSOURCE_HH

#include <string>
#include "utils/point3.hh"
#include "utils/color.hh"
#include "render/lightray.hh"

// predefinition to avoid non defined type in circling include
class Lightray;
using namespace std;

class Lightsource {

private:
    Point3 position;
    double intensity;
    Color* pColor;
    string name = "noname";

public:

    /* Constructors */
    Lightsource(Point3 position, double intensity, Color* color);

    /* Getters */
    Point3 get_position() const;
    double get_intensity() const;
    Color* get_pColor() const;

    /* Others */
    Point3 intersect (Lightray ray) const;

    string get_name() const;
    void set_name(string new_name);

    /* Debug */
    void print_debug();
};

#endif /* end of include guard: LIGHTSOURCE_HH */
