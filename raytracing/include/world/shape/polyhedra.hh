#ifndef POLYHEDRA_HH
#define POLYHEDRA_HH

#include <vector>
#include <string>
#include "utils/point3.hh"
#include "utils/vector3.hh"
#include "world/shape/shape.hh"
#include "world/shape/mesh.hh"
#include "render/lightray.hh"
#include "utils/csvloader.hh"


class Polyhedra: public Shape {

private:
    vector<Mesh*> vMesh;

public:

    Polyhedra(const vector<Mesh*>& vMesh, Material* material);
    Polyhedra(const string path, const Vector3& origin, Material* material);
    ~Polyhedra();
    Point3 intersect (const Lightray& ray) const;
    Vector3 get_normal (const Point3& p);
    bool is_in (const Point3& p) const;
    Material* get_pMaterial() const;

    /* Debug */
    void print_debug();

};

#endif /* end of include guard: POLYHEDRA_HH */
