#ifndef SHAPE
#define SHAPE

#include<string>
#include "render/lightray.hh"
#include "world/material.hh"
#include "utils/equation/equation.hh"
#include "utils/const.hh"

using namespace std;

class Shape {

protected:
    Material* pMaterial = NULL;
    string name = "noname";

public:
    /* Getters */
    EquationPoly get_equation() const;
    Material* get_pMaterial() const;

    /* Others */
    virtual Point3 intersect (const Lightray& ray) const  = 0 ;
    virtual Vector3 get_normal (const Point3& p) = 0;
    virtual bool is_in (const Point3& p) const = 0;

    string get_name() const;
    void set_name(string new_name);

    /* Destructors */
    virtual ~Shape() = 0;

    /* Debug */
    virtual void print_debug() = 0;
};

#endif /* end of include guard: SHAPE */
