#ifndef MESH_HH
#define MESH_HH

#include <vector>
#include "utils/point3.hh"
#include "utils/vector3.hh"
#include "world/shape/shape.hh"
#include "utils/const.hh"

using namespace std;

class Mesh : public Shape {

private:
    Vector3 normal;
    vector<Point3> vPoint;
public:
    Mesh(Vector3 normal, const vector<Point3>& vPoint, Material* material);
    ~Mesh();

    Point3 intersect (const Lightray& ray) const;
    Vector3 get_normal (const Point3& p);
    bool is_in (const Point3& p) const;
    /* Debug */
    void print_debug();
};

#endif /* end of include guard: MESH_HH */
