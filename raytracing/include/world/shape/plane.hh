#ifndef PLANE_HH
#define PLANE_HH

#include "world/shape/shape.hh"
#include "utils/equation/equation.hh"
#include "utils/point3.hh"
#include "utils/vector3.hh"
#include "utils/const.hh"
#include "render/lightray.hh"

class Plane : public Shape {

private:
    EquationPoly equation;

public:
    Plane(double coef_x, double coef_y, double coef_z, double cst, Material* material);
    ~Plane();
    EquationPoly get_equation() const;

    Point3 intersect (const Lightray& ray) const;
    Vector3 get_normal (const Point3& p);
    bool is_in (const Point3& p) const;

    /* Debug */
    void print_debug();
};

#endif /* end of include guard: PLANE_HH */
