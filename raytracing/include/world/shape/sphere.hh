#ifndef SPHERE_HH
#define SPHERE_HH

#include "world/shape/shape.hh"

class Sphere : public Shape {

private:
    EquationPoly equation;

public:
    Sphere(Point3 center, double radius, Material* material);
    ~Sphere();

    EquationPoly get_equation() const;

    Point3 intersect (const Lightray& ray) const;
    Vector3 get_normal (const Point3& p);
    bool is_in(const Point3& p) const;
    /* Debug */
    void print_debug();

};

#endif /* end of include guard: SPHERE_HH */
