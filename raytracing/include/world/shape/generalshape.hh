
#ifndef GENERALSHAPE_HH
#define GENERALSHAPE_HH

#include <vector>
#include "world/shape/shape.hh"
#include "utils/equation/equation.hh"
#include "utils/point3.hh"
#include "utils/vector3.hh"

class GeneralShape : public Shape {

private:
    EquationPoly equation;

public:
    GeneralShape(const vector<double>& v, Material* material);

    EquationPoly get_equation() const;
    Point3 intersect (const Lightray& ray) const;
    Vector3 get_normal (const Point3& p);
    bool is_in(const Point3& p) const;
    /* Debug */
    void print_debug();

};

#endif
