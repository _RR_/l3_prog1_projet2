# RayTracing 3D Renderer

This renderer was devellopped as a project for the prog1 class of the ENS Rennes.

## Build Status

This project is over. Final version was to be released by 03/12/2018 at 23h59 CEST.
The sources are given within the archive.

## Requirements

Latest version of C++ libraries and compiler are required (works with g++ and clang++).
Make is also required to compile all the sources together.

## Demo instructions

- Unzip the archive BastideCoudrayDuronPiau.zip
- Run `$ sh demo.sh` in the unzipped directory

## Build instructions

- Unzip the archive BastideCoudrayDuronPiau.zip
- Run `$ make` in the unziped directory
- Execute `$ ./raytracer` with the wanted parameters
> **Note :**  To learn more about possible parameters, just run `$ ./raytracer -h`

## Features of the renderer

This renderer works using the ray tracing method. It features :

- A classic 8-bits RGB color space.
- Management of diffusion, reflection and transparency of materials.
- Display of any surface parametrized by a polynomial equation.
- Display of any polyhedra using .stl files (using the ASCII convention).
- Easily create scenes with the csv world descriptor (see models/manual.nfo).

## Credits

This project was develloped by **Paul Bastide**, **Alex Coudray**,**Julien Duron** and **Rémi Piau**  based on an idea from Luc Bougé, teacher at the ENS Rennes.
You can also find our project repository on [Gitlab](https://gitlab.com/_RR_/l3_prog1_projet2.git)
