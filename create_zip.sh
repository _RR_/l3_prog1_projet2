#!/bin/sh

#check if code compile
cd raytracing;
make mrproper;
make indent;
make;
make mrproper;
cd ..;

# Now create archive
set -x
NAME=BastideCoudrayDuronPiau
mkdir -p $NAME
cp README.md $NAME/
cp demo.sh $NAME/
cp create_zip.sh $NAME/
cp -R raytracing $NAME/
zip -r BastideCoudrayDuronPiau.zip $NAME
rm -Rf $NAME
